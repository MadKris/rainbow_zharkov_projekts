package SSSS;


import javax.swing.*;
import java.awt.event.*;
import java.util.Random;

public class FrameWithButton {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Steam 2");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300, 200);

        JButton button = new JButton("Open Random Game");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Random random = new Random();
                int randomNumber = random.nextInt(2) + 1;

                switch (randomNumber) {
                    case 1:
                        new SnakeGme();
                        break;
                    case 2:
                        new TicTacToe();
                        break;
                }
            }
        });

        frame.add(button);
        frame.setVisible(true);
    }
}
