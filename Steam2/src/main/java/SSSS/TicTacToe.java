package SSSS;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;


public class TicTacToe extends JFrame implements ActionListener {
    private JButton[][] buttons = new JButton[3][3];
    private String player = "X";
    private JLabel label = new JLabel("Ход игрока " + player);

    public TicTacToe() {
        setTitle("Крестики-нолики");
        setSize(800, 900);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        JPanel panel = new JPanel(new GridLayout(3, 3));
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                buttons[i][j] = new JButton("");
                buttons[i][j].setFont(new Font("Arial", Font.BOLD, 80));
                buttons[i][j].addActionListener(this);
                panel.add(buttons[i][j]);
            }
        }
        add(panel, BorderLayout.CENTER);
        JPanel panel2 = new JPanel(new BorderLayout());
        panel2.add(label, BorderLayout.CENTER);
        JButton restartButton = new JButton("Новая игра");
        restartButton.addActionListener(this);
        panel2.add(restartButton, BorderLayout.SOUTH);
        add(panel2, BorderLayout.NORTH);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("Новая игра")) {
            resetGame();
            return;
        }
        JButton button = (JButton) e.getSource();
        if (button.getText().equals("")) {
            button.setText(player);
            if (isWinner()) {
                JOptionPane.showMessageDialog(this, "Игрок " + player + " выиграл!");
                resetGame();
            } else if (isBoardFull()) {
                JOptionPane.showMessageDialog(this, "Ничья!");
                resetGame();
            } else {
                player = player.equals("X") ? "O" : "X";
                label.setText("Ход игрока " + player);
            }
        }
    }

    private boolean isWinner() {
        String[][] board = new String[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = buttons[i][j].getText();
            }
        }
        for (int i = 0; i < 3; i++) {
            if (board[i][0].equals(board[i][1]) && board[i][1].equals(board[i][2]) && !board[i][0].equals("")) {
                return true;
            }
            if (board[0][i].equals(board[1][i]) && board[1][i].equals(board[2][i]) && !board[0][i].equals("")) {
                return true;
            }
        }
        if (board[0][0].equals(board[1][1]) && board[1][1].equals(board[2][2]) && !board[0][0].equals("")) {
            return true;
        }
        if (board[0][2].equals(board[1][1]) && board[1][1].equals(board[2][0]) && !board[0][2].equals("")) {
            return true;
        }
        return false;
    }

    private boolean isBoardFull() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (buttons[i][j].getText().equals("")) {
                    return false;
                }
            }
        }
        return true;
    }

    private void resetGame() {
        player = "X";
        label.setText("Ход игрока " + player);
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                buttons[i][j].setText("");
            }
        }
    }

    public static void main(String[] args) {
        new TicTacToe();
    }
}


